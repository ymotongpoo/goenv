#!/usr/bin/env zsh
#
# Environment variable GOENVGOROOT, GOENVTARGET is install target of `goenv`
# Default install target i /usr/local/bin.

if [ -z "$GOENVGOROOT" ]; then
  echo "[goenv] set GOENVGOROOT environment variable. (Go binary install target)"
  exit 0
fi

if [ -z "$GOENVTARGET" ]; then
  echo "[goenv] set GOENVTARGET environment variable. (goenv binary install target)"
  exit 0
fi

if [ ! -e "$GOENVGOROOT" -o ! -d "$GOENVGOROOT" ]; then
  if ! eval "mkdir -p $GOENVGOROOT"; then
    echo "[goenv] please confirm directory permission for GOENVGOROOT"      
    exit 0
  fi
fi

if [ ! -e "$GOENVTARGET" -o ! -d "$GOENVTARGET" ]; then
  if ! eval "mkdir -p $GOENVTARGET"; then
    echo "[goenv] please confirm directory permission for GOENVTARGET"      
    exit 0
  fi
fi

cd /tmp

downloader=""
if type curl &> /dev/null; then
  downloader="curl -O"
elif type wget &> /dev/null; then
  downloader="wget"
else
  echo "[goenv] `curl` or `wget` is required to fetch files."
  exit 0
fi

if eval "$downloader https://bitbucket.org/ymotongpoo/goenv/raw/master/shellscripts/goenvwrapper.sh"; then
  if [ -f "goenvwrapper.sh" ]; then
    source goenvwrapper.sh
  else
    echo "[goenv] cannot read goenvwrapper.sh"
    exit 0
  fi
else
  echo "[goenv] cannot download goenvwrapper.sh"
  exit 0
fi

if goof goinstall release; then
  if [ -d "$GOENVGOROOT/release" ]; then
    echo "[goenv] add following lines in your .barshrc/.zshrc"
    echo "export GOROOT=$GOENVGOROOT/release"
    echo "export PATH=$PATH:$GOROOT/bin"
    export GOROOT="$GOENVGOROOT/release"
    export PATH="$GOENVGOROOT/release/bin:$PATH"
  fi
else
  echo "[goenv] `goof goinstall` command does not work."
  exit 0
fi

if ! type git 2>&1 > /dev/null; then
  echo "[goenv] To get goenv, install git"
  exit 0
fi

echo "[goenv] cloning goenv from GitHub"
if [ -d goenv ]; then
  if ! eval "rm -rf goenv"; then
    echo "[goenv] please confirm directory permission for /tmp/goenv"
    exit 0
  fi
fi
if git clone https://bitbucket.org/ymotongpoo/goenv.git; then
  cd goenv
  if ! go build -o goenv; then
    echo "[goenv] cannot build goenv"
  fi
  if ! cp goenv "$GOENVTARGET"; then
    echo "[goenv] cannot copy goenv. check permission of '$GOENVTARGET'"
  fi
  if ! cp shellscripts/goenvwrapper.sh $GOENVTARGET; then
    echo "[goenv] failed copying goenvwrapper.sh. check permission of '$GOENVTARGET'"
  fi
else
  echo "[goenv] cannot clone goenv repository"
  exit 0
fi

echo "[goenv] install finished."
exit 0
